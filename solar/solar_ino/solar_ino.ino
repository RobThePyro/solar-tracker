/*

/* 
 * Arduino 101: timer and interrupts
 * 3: Timer2 compare interrupt example. Quadrature Encoder
 * more infos: http://www.letmakerobots.com/node/28278
 * created by RobotFreak 
 *
 * Credits:
 * based on code from Peter Dannegger
 * http://www.mikrocontroller.net/articles/Drehgeber
 */

#define SHOOTDLY 5 //ms delay to prevent shoot through

#define DEBUG // includes the debug code.. 

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WConstants.h"
#endif

// Motor Pins
#define AH  3
#define AL  5
#define BH  4
#define BL  6

// input pins
#define inputPin  A0

// Encoder Pins
#define encLtA 2
#define encLtB 3
#define encRtA 11 
#define encRtB 12
#define ledPin 13

#define LT_PHASE_A		digitalRead(encLtA)
#define LT_PHASE_B		digitalRead(encLtB)
#define RT_PHASE_A		digitalRead(encRtA)
#define RT_PHASE_B		digitalRead(encRtB)

static volatile int8_t encDeltaLt, encDeltaRt;
static int8_t lastLt, lastRt;
int encLt, encRt;
int refValue = 0;
int sensValue = 0;
int oldRate = 0;
int newRate = 0;
long oldtime = 0;

ISR( TIMER2_COMPA_vect )
{
  int8_t val, diff;

  digitalWrite(ledPin, HIGH);   // toggle LED pin
  val = 0;
  if( LT_PHASE_A )
    val = 3;
  if( LT_PHASE_B )
    val ^= 1;					// convert gray to binary
  diff = lastLt - val;				// difference last - new
  if( diff & 1 ){				// bit 0 = value (1)
    lastLt = val;				// store new as next last
    encDeltaLt += (diff & 2) - 1;		// bit 1 = direction (+/-)
  }

  val = 0;
  if( RT_PHASE_A )
    val = 3;
  if( RT_PHASE_B )
    val ^= 1;					// convert gray to binary
  diff = lastRt - val;				// difference last - new
  if( diff & 1 ){				// bit 0 = value (1)
    lastRt = val;				// store new as next last
    encDeltaRt += (diff & 2) - 1;		// bit 1 = direction (+/-)
  }
  digitalWrite(ledPin, LOW);   // toggle LED pin
}


void QuadratureEncoderInit(void)
{
  int8_t val;

  cli();
  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  
  OCR2A = 63;            // compare match register 16MHz/64/4000Hz 
  TCCR2A |= (1 << WGM21);   // CTC mode
  TCCR2B |= (1 << CS21);    // 64 prescaler 
  TCCR2B |= (1 << CS20);    // 64 prescaler 
  TIMSK2 |= (1 << OCIE2A);  // enable timer compare interrupt
  
  sei();
  pinMode(encLtA, INPUT);
  pinMode(encRtA, INPUT);
  pinMode(encLtB, INPUT);
  pinMode(encRtB, INPUT);

  val=0;
  if (LT_PHASE_A)
    val = 3;
  if (LT_PHASE_B)
    val ^= 1;
  lastLt = val;
  encDeltaLt = 0;

  val=0;
  if (RT_PHASE_A)
    val = 3;
  if (RT_PHASE_B)
    val ^= 1;
  lastRt = val;
  encDeltaRt = 0;

  encLt = 0;
  encRt = 0;
}

int8_t QuadratureEncoderReadLt( void )			// read single step encoders
{
  int8_t val;

  cli();
  val = encDeltaLt;
  encDeltaLt = 0;
  sei();
  return val;					// counts since last call
}

int8_t QuadratureEncoderReadRt( void )			// read single step encoders
{
  int8_t val;

  cli();
  val = encDeltaRt;
  encDeltaRt = 0;
  sei();
  return val;					// counts since last call
}

void H_Init(){
  // set pins as outputs
  pinMode(AL, OUTPUT);
  pinMode(AH, OUTPUT);
  pinMode(BL, OUTPUT);
  pinMode(BH, OUTPUT);
  
  // make sure they are all off. 
  digitalWrite(AL, HIGH);
  digitalWrite(AH, LOW);
  digitalWrite(BL, HIGH);
  digitalWrite(BH, LOW);
}



void Drive_Motor(int rate){ // drives the motor at a specific speed, percent from -100 to 100
  // first decide direction:
  if(rate > 0){ // go right
    rate = map(rate, 0,100, 150, 255); //scale nicely
    if (rate > 255) rate = 255; // error checking
    
    right(rate);

  }

    
  else if(rate == 0){
    off();
  }
    
  else{ // rate < 0
    rate = map(rate, -100, 0, 255, 150);
    if (rate < 150) rate = 150;

    left(rate);
    
  } 
}
    
    

void right(int rate){ // drives the motor right at speed: rate 0-255
  digitalWrite(BH, 0);
  digitalWrite(AL,1);
  delay(SHOOTDLY);  
  digitalWrite(AH,1);
  delay(SHOOTDLY);
  analogWrite(BL,255-rate);
  
}

void left(int rate){ // drives the motor left at speed: rate 0-255
  digitalWrite(AH, 0);
  digitalWrite(BL,1);
  delay(SHOOTDLY);
  digitalWrite(BH,1);
  delay(SHOOTDLY);
  analogWrite(AL,255-rate);
}

void off(void){ //turns off all mosfets!
  digitalWrite(AL, HIGH);
  digitalWrite(AH, LOW);
  digitalWrite(BL, HIGH);
  digitalWrite(BH, LOW);  
}

void setup()
{
  H_Init(); // setup motor driver first! DANGER! 
  
  #ifdef DEBUG
  Serial.begin(38400);
  #endif
  
  pinMode(ledPin, OUTPUT);
  pinMode(inputPin, INPUT);
  QuadratureEncoderInit(); // setup the encoders
}

int Controller(int ref, int sens){
  #define THRESHOLD  2
  int error = ref - sens; //-ve if too far, +ve if too close
  error = map(error, 5000, -5000, -100, 100); // map to 0-1023
  // do some hysterisis
  if(error > -THRESHOLD && error < THRESHOLD){
    error = 0;
  }
  
  return error; 
}


void loop()
{
  encLt += QuadratureEncoderReadLt(); // read the two encoders and get latest position
  encRt += QuadratureEncoderReadRt();
  
  sensValue = encRt;
  
  refValue = analogRead(inputPin); // get input from the pot
  refValue = map(refValue, 0,1023,0,-5000); // maps the pot to points on the encoders range
  
  oldRate = newRate; // store the oldRate
  newRate = Controller(refValue, sensValue); // calculate the new rate
  
  if(newRate != oldRate){ // if the rate has changed,
   Drive_Motor(newRate); // tell the motor!
  }
   

#ifdef DEBUG
if(millis() - oldtime >100){ // update only every 100ms
  oldtime = millis(); 
  Serial.print("Lt: ");
  Serial.print(encLt, DEC);
  Serial.print(" Rt: ");
  Serial.print(encRt, DEC);
  Serial.print(" setpoint: ");
  Serial.print(refValue, DEC);
  Serial.print(" Rate: ");
  Serial.println(newRate, DEC);
}
  
#endif
}



